(ns quipper.core
  (require [clojure.tools.cli :as cli])
  (:gen-class))

(def cli-options [["-h" "--help" "Print this help info"
                   :default false :flag true]
                  ["-f" "--file FILE" "Specify an alternate quip file."
                   :default (str
                             (System/getProperty "user.home")
                             "/.quips")]])


(defn print-banner [summary]
  (println "This is the 'quipper' program.\n" summary))

(defn- add [file quips]
  (if (empty? quips)
    (print-banner nil)
    (spit file (first quips))))

(defn parse-commands [file [command & quips]]
  (case command
    "add" (add file quips)
    "remove" (println "removing " quips)))


(defn -main
  "Deal with quips."
  [& args]

  (let [{:keys [options arguments summary errors] :as opts}
        (cli/parse-opts args cli-options)]
;;     (println (pr-str opts))
    (cond
     (:help options) (print-banner summary)
      :else (parse-commands (:file options) arguments))))



;;   (let [{:keys [options arguments summary errors]}
;;         (cli/parse-opts args cli-options)])
;;   (println "You'll need to implement this yourself."))

